<%@tag description="put the tag description here" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<!-- Definisce una variabile boostrapCss che contiene l'url del css -->
<spring:url value="/assets/css/bootstrap.min.css" var="boostrapCss" htmlEscape="true"/>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <title>Spring Spike</title>
        <link rel="stylesheet" type="text/css" href="${boostrapCss}"/>
    </head>
    <body>
        <div class="container">
            <!-- Questo permette di inserire del codice jsp dall'esterno 
                 dove questo tag viene chiamato
            -->
            <jsp:doBody/>
        </div>
    </body>
</html>