<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<spring:url value="/" var="home" htmlEscape="true"/>
<tags:layout>
    <div class="row">
        <div class="pagination-centered header">
            <h1><a href="${home}">Spring Spike</a></h1> 
        </div>
    </div>
    <spring:bind path="newUser">
        <c:if test="${status.error}">
            <c:forEach var="errorMessage" items="${status.errorMessages}">
                <div class="alert alert-error">
                     <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <h4>Oh snap!</h4>
                    ${errorMessage}
                </div>
            </c:forEach>
        </c:if>
    </spring:bind>
    <div class="row">
        <div class="span6 offset3">
            <form:form class="form-horizontal" method="POST" action='sing-up' commandName="newUser" > 
                <div class="control-group">
                    <label class="control-label" for="inputEmail">Email</label>
                    <div class="controls">
                        <form:input path="email" id="inputEmail" placeholder=" Email" type="email" required="required" />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="inputUsername">Username</label>
                    <div class="controls">
                        <form:input path="username" id="inputUsername" placeholder="Username" required="required" />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="inputPassword">Password</label>
                    <div class="controls">
                        <form:password path="password" id="inputPassword" placeholder="Password"/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="inputConfirmPassword">Confirm Password</label>
                    <div class="controls">
                        <form:password path="confirmPassword" id="inputConfirmPassword" placeholder="Confirm Password"/>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <button type="submit" class="btn">Sign me up</button>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</tags:layout>
