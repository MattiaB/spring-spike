package org.unito.springspike.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 
 * @author Mattia Bertorello
 */
@Controller
public class IndexController {
    
    // Questa annotazione definisce che questo metodo viene chiamato alla root 
    // del sito
   @RequestMapping("/")
   // L'oggetto model è un HashMap dove ogni chiave corrisponde ad una variabile
   // che sarà presente nella jsp
    public String index(ModelMap model) {
        model.addAttribute("test", "Questa è una variabile presente nella index.jsp");
        // La stringa rappresenta la jsp che viene elaborata dopo l'esecuzione 
        // di questo metodo        
        return "index";
    }
}
