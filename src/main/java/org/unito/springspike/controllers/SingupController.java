package org.unito.springspike.controllers;

import javax.servlet.http.HttpSession;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.unito.springspike.model.User;

/**
 *
 * @author Mattia Bertorello
 */
@Controller
// Il tutte le @RequestMapping all'interno del controller partono da /user
// quindi per esempio la url può essere http://localhost:8080/SpringSpike/user
@RequestMapping("/user")
// Per collegare il tag form di spring e il controller bisogna stare attenti 
// a usare sempre lo stesso nome per la variabile che conterrà i dati del form.
// 1. Jsp bisogna aggiungere il parametro commandName="newUser" nel tag "form"
// 2. Rrichiesta GET si inserisce nel model model.put("newUser", new User());
//      un oggetto utente vuoto.
// 3. Richiesta POST (@ModelAttribute("newUser")) all'attributo "newUser" è associato
//      l'oggetto user che contiene i dati del form
public class SingupController {

    private static final Log log = LogFactory.getLog(SingupController.class);

    // Questo metodo viene chiamato quando la richiesta è di tipo GET e la possibile
    // url è http://localhost:8080/SpringSpike/user/sing-up
    @RequestMapping(value = "/sing-up", method = RequestMethod.GET)
    public String singUpNew(ModelMap model) {
        model.put("newUser", new User());
        return "sing-up";
    }

    // Questo metodo viene chiamato con la stessa url del metodo precedente ma 
    // la richiesta deve essere di tipo POST
    @RequestMapping(value = "/sing-up", method = RequestMethod.POST)
    // BindingResult result serve per gestire gli errori di validazione nel caso fossero presenti
    public String createUser(@ModelAttribute("newUser") User user, BindingResult result, SessionStatus status, HttpSession session) {
        log.info("Username:" + user.getUsername() + " Password:" + user.getPassword() + " Email:" + user.getEmail() + " Confirm Password:" + user.getConfirmPassword());
        if (user.getPassword().equals(user.getConfirmPassword())) {
            log.info("Creato utente di nome: " + user.getUsername());
            // Inserisce un attributo nella sessione
            session.setAttribute("currentUser", user);
            // Fissa come completata la modifica della sessione, se non lo si esegue 
            // le modifiche nella sessione non vengono salvate
            status.setComplete();
        } else {
            result.reject("password", "Le password non sono uguali");
            return "sing-up";
        }
        return "redirect:/";
    }
}
